# EveningWm

Less features, less problems

![](https://preview.redd.it/zy0hoxsicvx11.png?width=960&crop=smart&auto=webp&s=0fd9ce892e57e8a8631af4543e48f4b6e42558b3)

### What is EveningWm?
EveningWm is a super tiny window manager written in C using Xlib. The main goal of this wm is to keep the user focused on his/her work and to stay away from making constant changes to the window manager. EveningWm is currently only 78 SLOC (generated using sloccount.)

This project is a proud fork of the world's smallest wm,  [TinyWm](http://incise.org/tinywm.html)
(originally written by Nick Welch <nick@incise.org> in 2005 & 2011.)

### How do I use EveningWm?
* Sloppy focus follows the mouse
* Move and focus a window with Alt + Mouse1
* Resize and focus a window with Alt + Mouse3
* Raise a window with Alt + r
* Lower a window with Alt + d
* Launch a terminal with Alt + Return (default is urxvt)
* Launch a browser with Alt + f (default is firefox)
* Close EveningWm with Alt + 0
* EveningWm ignores bars that set the override_redirect value

### Configuration
There isn't really much to configure, but if you wan't to change the keybinds, you simply edit the source code. Don't worry, it won't bite you ;)

### Build requirements
* gcc
* make
* Xlib headers

### Installation
To install EveningWm run:
```
make all
```

and to uninstall:
```
make clean
```

### Known bugs/features to come 
* Focusing windows when they appear would be nice
* Currently there isn't a way to tell bars/docks to **always** stay above other windows
* If any modifier key in addition to Alt (e.g caps lock, num lock..) is pressed the keybinds won't work

### Contributing
Feel free to send merge reguests if you found a way to fix a problem or if you came up with some new features that you think would fit the "style" of EveningWm

### Thanks to these projects for help
* Neutralwm
* dwm
* TinyWm (of course)

### Warning
EveningWm is my first actual C project ever, so expect the worst but hope for the best.